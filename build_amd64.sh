#!/bin/bash

export GPG_TTY=$(tty)

echo "$KEY" > priv.gpg
gpg --import pub.gpg && gpg --batch --import priv.gpg

version="$(wget https://api.github.com/repos/ProtonMail/proton-bridge/releases/latest -O - -q | grep tag_name | sed 's/.*v/v/' | sed 's/".*//')"

if [[ $(wget https://julianfairfax.codeberg.page/proton-bridge-prebuilt/amd64/version.txt -O -) == $version ]]; then
	mkdir -p public/amd64

	cd public/amd64

	wget https://julianfairfax.codeberg.page/proton-bridge-prebuilt/amd64/proton-bridge

	if [[ ! -f proton-bridge ]]; then
		wget https://julianfairfax.codeberg.page/proton-bridge-prebuilt/amd64/proton-bridge
	fi

	wget https://julianfairfax.codeberg.page/proton-bridge-prebuilt/amd64/proton-bridge.asc
	
	wget https://julianfairfax.codeberg.page/proton-bridge-prebuilt/amd64/bridge

	if [[ ! -f bridge ]]; then
		wget https://julianfairfax.codeberg.page/proton-bridge-prebuilt/amd64/bridge
	fi

	wget https://julianfairfax.codeberg.page/proton-bridge-prebuilt/amd64/bridge.asc
	
	cd ../../
else
	git clone https://github.com/ProtonMail/proton-bridge

	cd proton-bridge

	git fetch --tags

	git checkout tags/$version

	make build-nogui

	mkdir -p ../public/amd64

	cp ../pub.gpg ../public

	gpg --detach-sig --armor --sign proton-bridge

	gpg --detach-sig --armor --sign bridge

	cp proton-bridge ../public/amd64/

	cp proton-bridge.asc ../public/amd64/

	cp bridge ../public/amd64/

	cp bridge.asc ../public/amd64/
	
	cd ../
	
	rm -rf proton-bridge
fi

echo "$version" | tee public/amd64/version.txt
	
echo "$version" | tee public/version.txt
