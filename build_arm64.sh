#!/bin/bash

export GPG_TTY=$(tty)

echo "$KEY" > priv.gpg
gpg --import pub.gpg && gpg --batch --import priv.gpg

version="$(wget https://api.github.com/repos/ProtonMail/proton-bridge/releases/latest -O - -q | grep tag_name | sed 's/.*v/v/' | sed 's/".*//')"

if [[ $(wget https://julianfairfax.codeberg.page/proton-bridge-prebuilt/arm64/version.txt -O -) == $version ]]; then
	mkdir -p public/arm64

	cd public/arm64

	wget https://julianfairfax.codeberg.page/proton-bridge-prebuilt/arm64/proton-bridge

	if [[ ! -f proton-bridge ]]; then
		wget https://julianfairfax.codeberg.page/proton-bridge-prebuilt/arm64/proton-bridge
	fi

	wget https://julianfairfax.codeberg.page/proton-bridge-prebuilt/arm64/proton-bridge.asc
	
	wget https://julianfairfax.codeberg.page/proton-bridge-prebuilt/arm64/bridge

	if [[ ! -f bridge ]]; then
		wget https://julianfairfax.codeberg.page/proton-bridge-prebuilt/arm64/bridge
	fi

	wget https://julianfairfax.codeberg.page/proton-bridge-prebuilt/arm64/bridge.asc
	
	cd ../../
else
	git clone https://github.com/ProtonMail/proton-bridge

	cd proton-bridge

	git fetch --tags

	git checkout tags/$version

	GOARCH=arm64 CGO_ENABLED=1 CC=/usr/bin/aarch64-linux-gnu-gcc-12 PKG_CONFIG_PATH=/usr/lib/aarch64-linux-gnu/pkgconfig make build-nogui

	mkdir -p ../public/arm64

	cp ../pub.gpg ../public

	gpg --detach-sig --armor --sign proton-bridge

	gpg --detach-sig --armor --sign bridge

	cp proton-bridge ../public/arm64/

	cp proton-bridge.asc ../public/arm64/

	cp bridge ../public/arm64/

	cp bridge.asc ../public/arm64/
	
	cd ../
	
	rm -rf proton-bridge
fi

echo "$version" | tee public/arm64/version.txt
